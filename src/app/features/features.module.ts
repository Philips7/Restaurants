import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {RestaurantsModule} from './restaurants';
import {UsersModule} from './users';

@NgModule({
  imports: [
    CommonModule,
    RestaurantsModule,
    UsersModule
  ],
  declarations: [],
  exports: []
})
export class FeaturesModule { }
