import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import {UsersRoutingModule} from './users-routing.module';
import {LoginComponent, SettingsComponent} from './page';
import {SettingsMainComponent} from './components';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UsersRoutingModule
  ],
  declarations: [
    LoginComponent,
    SettingsComponent,
    SettingsMainComponent
  ]
})
export class UsersModule {
}
