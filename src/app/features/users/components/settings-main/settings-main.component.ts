import {ChangeDetectionStrategy, Component} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {Address, IUserDetails, UserDetails} from '../../../../core';
import {
  AppState,
  GetUserDetails,
  IUpdateAddressWithName,
  PostUserAddress,
  PostUserData,
  RemoveUserAddress,
  selectUserDetailsAll,
  UpdateUserAddress,
  UpdateUserAddressWithName
} from '../../../../store';
import {ValidateAge} from '../../../../shared';
import {IAddress} from '../../../../core/interfaces';

@Component({
  selector: 'app-settings-main',
  templateUrl: './settings-main.component.html',
  styleUrls: ['./settings-main.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsMainComponent {

  selectedAddress: Address;
  userDetails$: Observable<UserDetails>;
  userForm: FormGroup;

  constructor(private store: Store<AppState>, private fb: FormBuilder) {
    this.createForm();
    this.store.dispatch(new GetUserDetails());
    this.userDetails$ = this.store.select(selectUserDetailsAll);
    const emptyAddr: IAddress = {
      name: '',
      street: '',
      city: '',
      state: '',
      zipCode: ''
    };
    this.selectedAddress = new Address(emptyAddr);

    this.userDetails$.filter(data => data.user !== null).take(1)
      .subscribe(data => {
        this.userForm.patchValue({
          details: {
            name: data.user!.name,
            age: data.user!.age,
            sex: data.user!.sex
          },
        });
      });
  }

  createForm() {
    this.userForm = this.fb.group({
      details: this.fb.group({
        name: ['', [Validators.required]],
        age: ['', [Validators.required, ValidateAge]],
        sex: ['', [Validators.required]]
      })
    });
  }

  select(address: Address) {
    this.selectedAddress = address;
  }

  createAddress(address: Address) {
    this.store.dispatch(new PostUserAddress(address));
  }

  updateAddress(address: Address) {
    this.store.dispatch(new UpdateUserAddress(address));
  }

  updateAddressWithName(updatedAddress: IUpdateAddressWithName) {
    this.store.dispatch(new UpdateUserAddressWithName(updatedAddress));
  }

  removeAddress(addressName: string) {
    this.store.dispatch(new RemoveUserAddress(addressName));
  }

  updateUserData() {
    const result = this.userForm.valid;
    const user: UserDetails = this.userForm.value.details;
    const userData: IUserDetails = new UserDetails(user);
    this.userForm.patchValue({
      details: {
        name: user.name,
        age: user.age,
        sex: user.sex
      },
    });
    this.store.dispatch(new PostUserData(userData));
  }

}
