import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {SettingsMainComponent} from './components';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: SettingsMainComponent
      }
    ])
  ],
  declarations: [],
  providers: [],
  exports: [
    RouterModule
  ]
})

export class UsersRoutingModule {
}
