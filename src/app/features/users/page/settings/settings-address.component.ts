import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output
} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import {Address} from '../../../../core';
import {IUpdateAddressWithName} from '../../../../store';
import {IAddress} from '../../../../core/interfaces';

@Component({
  selector: 'app-settings',
  templateUrl: './settings-address.component.html',
  styleUrls: ['./settings-address.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SettingsComponent implements OnChanges {

  userForm: FormGroup;
  @Input() selectedAddress: Address;
  @Output() addressToUpdate = new EventEmitter<Address>();
  @Output() addressToCreate = new EventEmitter<Address>();
  @Output() addressToRemove = new EventEmitter<string>();
  @Output() addressToUpdateWithName = new EventEmitter<IUpdateAddressWithName>();

  constructor(private fb: FormBuilder) {
    this.clearAddress();
    this.createForm();
  }

  ngOnChanges() {
    this.userForm.reset({
        name: this.selectedAddress!.name,
        state: this.selectedAddress!.state,
        city: this.selectedAddress!.city,
        street: this.selectedAddress!.street,
        zipCode: this.selectedAddress!.zipCode,
    });
  }

  createForm() {
    this.userForm = this.fb.group({
      name: ['', [Validators.required]],
      street: ['', [Validators.required]],
      city: ['', [Validators.required]],
      state: ['', [Validators.required]],
      zipCode: ['', [Validators.required]]
    });
  }

  updateAddress() {
    const address: Address = this.userForm.value;
    if (address.name !== this.selectedAddress!.name) {
      const updatedAddress: IUpdateAddressWithName = {
        singleAddress: address,
        deletedName: this.selectedAddress!.name
      };
      this.addressToUpdateWithName.emit(updatedAddress);
    } else {
      this.addressToUpdate.emit(address);
    }
    this.selectedAddress!.name = address.name;
    this.userForm.patchValue({
      name: address.name,
      street: address.street,
      city: address.city,
      state: address.state,
      zipCode: address.zipCode,
    });
  }

  addAddress() {
    const address: IAddress = {
      name: this.userForm.value.name,
      street: this.userForm.value.street,
      city: this.userForm.value.city,
      state: this.userForm.value.state,
      zipCode: this.userForm.value.zipCode,
    };
    this.addressToCreate.emit(new Address(address));
    this.clearAddress();
    this.userForm.reset();
  }

  clean() {
    this.userForm.reset();
  }

  createNew() {
    this.userForm.reset();
    this.clearAddress();
  }

  deleteAddress(name: string) {
    this.addressToRemove.emit(name);
    this.userForm.reset();
    this.clearAddress();
  }

  private clearAddress() {
    const emptyAddr: IAddress = {
      name: '',
      street: '',
      city: '',
      state: '',
      zipCode: ''
    };
    this.selectedAddress = new Address(emptyAddr);
  }

}
