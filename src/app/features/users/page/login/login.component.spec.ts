import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {Store, StoreModule} from '@ngrx/store';
import {RouterModule} from '@angular/router';
import {By} from '@angular/platform-browser';

import {geocodeReducer, restaurantReducer, userReducer, GeoGeocodeSuccess} from '../../../../store';
import {GeoCode} from '../../../../core';
import {RestaurantDetailsComponent} from '../restaurant-details/restaurant-details.component';
import {RestaurantsListComponent} from './restaurants-list.component';

describe('RestaurantsListComponent', () => {
  let component: RestaurantsListComponent;
  let fixture: ComponentFixture<RestaurantsListComponent>;
  let store: Store<any>;
  let geocodeFixture: any;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'restaurants/:id', component: RestaurantDetailsComponent}
        ]),
        StoreModule.forRoot({
          user: userReducer,
          geocode: geocodeReducer,
          restaurant: restaurantReducer
        }),
        RouterModule
      ],
      declarations: [
        RestaurantsListComponent,
        RestaurantDetailsComponent
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RestaurantsListComponent);
    component = fixture.componentInstance;
    store = fixture.debugElement.injector.get(Store);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not load on start', () => {
    component.geocode$.subscribe(data => {
      expect(data.loading).toBe(false);
    });
  });

  it('should set default table info', () => {
    fixture.detectChanges();
    spyOn(component, 'setTableInfo');
    component.setTableInfo();
    expect(component.setTableInfo).toHaveBeenCalled();
  });


  it('should check component data setup', () => {
    geocodeFixture = {
      'location': {
        'entity_type': 'subzone',
        'entity_id': 97456,
        'title': 'Rzeszów',
        'latitude': 50.0499990000,
        'longitude': 22.0000000000,
        'city_id': 267,
        'city_name': 'Rzeszów',
        'country_id': 163,
        'country_name': 'Poland'
      },
      'popularity': {
        'popularity': '2.97',
        'nightlife_index': '2.25',
        'nearby_res': [
          '16534018',
          '16533762',
          '16533772',
          '18241612',
          '16533774',
          '16534113',
          '16534081',
          '16534070',
          '16533798'
        ],
        'top_cuisines': [
          'Polish',
          'Drinks Only',
          'Pizza',
          'Cafe',
          'Finger Food'
        ],
        'popularity_res': '100',
        'nightlife_res': '10',
        'subzone': 'Rzeszów',
        'subzone_id': 97456,
        'city': 'Rzeszów'
      },
      'link': 'https://www.zomato.com/rzeszów/rzeszów-restaurants',
      'nearby_restaurants': [
        {
          'restaurant': {
            'R': {
              'res_id': 16534018
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16534018',
            'name': 'Dara Kebab',
            'url': 'https://www.zomato.com/rzeszów/dara-kebab-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 18, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0396170000',
              'longitude': '22.0028890000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Kebab, Turkish',
            'average_cost_for_two': 35,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '28'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/dara-kebab-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/dara-kebab-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16534018',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/dara-kebab-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16533762
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16533762',
            'name': 'Cukiernia Wiedeńska - Hotel Ambasadorski',
            'url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 13-14, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0372540000',
              'longitude': '22.0056600000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Desserts, Grill, Cafe',
            'average_cost_for_two': 35,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.3',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '14'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16533762',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16533772
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16533772',
            'name': 'Ceska Hospoda',
            'url': 'https://www.zomato.com/rzeszów/ceska-hospoda-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 5, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0371800000',
              'longitude': '22.0046500000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Czech, Polish',
            'average_cost_for_two': 35,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '45'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/ceska-hospoda-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/ceska-hospoda-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16533772',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/ceska-hospoda-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 18241612
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '18241612',
            'name': 'Radość',
            'url': 'https://www.zomato.com/rzeszów/rado-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 24, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0378680000',
              'longitude': '22.0044360000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Burger, Polish, European',
            'average_cost_for_two': 75,
            'price_range': 2,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '6'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/rado-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/rado-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/18241612',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/rado-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16533774
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16533774',
            'name': 'Konfitura',
            'url': 'https://www.zomato.com/rzeszów/konfitura-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Słowackiego 8, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0371420000',
              'longitude': '22.0034100000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Polish, Healthy Food',
            'average_cost_for_two': 0,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '29'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/konfitura-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/konfitura-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16533774',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/konfitura-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16534113
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16534113',
            'name': 'Wesele - Hotel Bristol',
            'url': 'https://www.zomato.com/rzeszów/wesele-hotel-bristol-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 20-23, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0376080000',
              'longitude': '22.0055260000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Polish, International',
            'average_cost_for_two': 0,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '8'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/wesele-hotel-bristol-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/wesele-hotel-bristol-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16534113',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/wesele-hotel-bristol-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16534081
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16534081',
            'name': 'Stary Browar Rzeszowski',
            'url': 'https://www.zomato.com/rzeszów/stary-browar-rzeszowski-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 20-23, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0376080000',
              'longitude': '22.0055260000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Finger Food, Polish',
            'average_cost_for_two': 75,
            'price_range': 2,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.3',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '33'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/stary-browar-rzeszowski-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/stary-browar-rzeszowski-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16534081',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/stary-browar-rzeszowski-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16534070
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16534070',
            'name': 'Ararat Kebab',
            'url': 'https://www.zomato.com/rzeszów/ararat-kebab-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 9, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0371130000',
              'longitude': '22.0050470000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Kebab',
            'average_cost_for_two': 35,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '11'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/ararat-kebab-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/ararat-kebab-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16534070',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/ararat-kebab-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 16533798
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16533798',
            'name': 'Grzesznicy',
            'url': 'https://www.zomato.com/rzeszów/grzesznicy-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': '3 Maja 7, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0370500000',
              'longitude': '22.0016160000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Polish',
            'average_cost_for_two': 0,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '40'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/grzesznicy-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/grzesznicy-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16533798',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/grzesznicy-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        }
      ]
    };

    const geocodeResponse = new GeoCode(geocodeFixture.link,
      geocodeFixture.location, geocodeFixture.nearby_restaurants);
    store.dispatch(new GeoGeocodeSuccess(geocodeResponse));

    component.geocode$.subscribe((data) => {
      expect(data.loading).toBe(false);
      expect(data!.nearby_restaurants!.length)
        .toBe(geocodeFixture.nearby_restaurants.length);
    });
  });

  it('should test html for valid content', () => {
    const debugName = fixture.debugElement.queryAll(By.css('#name'));
    debugName.forEach((element, index) => {
      expect(element.nativeElement.textContent)
        .toEqual(geocodeFixture.nearby_restaurants[index].restaurant.name);
    });

    const debugRating = fixture.debugElement.queryAll(By.css('#rating'));
    debugRating.forEach((element, index) => {
      expect(element.nativeElement.textContent)
        .toEqual(geocodeFixture.nearby_restaurants[index].restaurant.user_rating.aggregate_rating);
    });

    const debugAddress = fixture.debugElement.queryAll(By.css('#address'));
    debugAddress.forEach((element, index) => {
      expect(element.nativeElement.textContent)
        .toEqual(geocodeFixture.nearby_restaurants[index].restaurant.location.address);
    });

    it('should test click event with navigation', () => {
      const debugClick = fixture.debugElement.query(By.css('#name'));
      spyOn(component, 'navToDetails');
      debugClick.nativeElement.click();
      expect(component.navToDetails).toHaveBeenCalledWith(16534018);
      expect(component.navToDetails).toHaveBeenCalledTimes(1);
    });

  });

});
