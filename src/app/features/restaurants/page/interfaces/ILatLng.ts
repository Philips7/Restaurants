export interface ILatLng {
  lat: number;
  lon: number;
}
