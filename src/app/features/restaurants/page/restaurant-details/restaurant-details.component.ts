import {ChangeDetectionStrategy, Component} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs/Observable';

import {Restaurant} from '../../../../core';


@Component({
  selector: 'app-restaurant-details',
  templateUrl: './restaurant-details.component.html',
  styleUrls: ['./restaurant-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RestaurantDetailsComponent {

  restaurant$: Observable<Restaurant>;

  constructor(private route: ActivatedRoute) {
    this.restaurant$ = this.route.snapshot.data['restaurant'];
  }

}
