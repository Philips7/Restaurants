import {Component, Input} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {Store} from '@ngrx/store';
import {Router} from '@angular/router';

import {
  AppState,
  GetGeocode,
  GetSearch,
  selectGeocodeAll,
  selectSearchAll,
  selectSearchResultsFound,
  selectSearchResultsShown,
  selectSearchResultsStart
} from '../../../../store';
import {GeoCode, Search} from '../../../../core';
import {ILatLng} from '../interfaces';

@Component({
  selector: 'app-restaurants-list',
  templateUrl: './restaurants-list.component.html',
  styleUrls: ['./restaurants-list.component.scss']
})
export class RestaurantsListComponent {

  readonly LAT = 40.730610;
  readonly LNG = -73.935242;

  geocode$: Observable<GeoCode>;
  search$: Observable<Search>;
  count$: Observable<number>;
  perPage$: Observable<number>;
  start$: Observable<number>;
  page: number;
  @Input() topTen: boolean;
  @Input() location: ILatLng;

  constructor(private store: Store<AppState>, private router: Router) {
    this.page = 1;
    this.location = {lat: this.LAT, lon: this.LNG};
    this.geocode$ = this.store.select(selectGeocodeAll);
    this.search$ = this.store.select(selectSearchAll);
    this.count$ = this.store.select(selectSearchResultsFound);
    this.perPage$ = this.store.select(selectSearchResultsShown);
    this.start$ = this.store.select(selectSearchResultsStart);
    this.store.dispatch(new GetSearch({lat: this.location.lat, lon: this.location.lon, start: 0}));
    this.store.dispatch(new GetGeocode({lat: this.location.lat, lon: this.location.lon}));
  }

  navToDetails(id: number) {
    this.router.navigateByUrl(`/restaurants/${id}`);
  }

  nexRestaurants() {
    if (this.nextPageExists()) {
      this.store.dispatch(new GetSearch({
        lat: this.location.lat,
        lon: this.location.lon,
        start: this.page * 20
      }));
      this.page = this.page + 1;
    }
  }

  prevRestaurants() {
    if (this.prevPageExists()) {
      this.store.dispatch(new GetSearch({
        lat: this.location.lat,
        lon: this.location.lon,
        start: (this.page * 20 ) - 40
      }));
      this.page = this.page - 1;
    }
  }

  nextPageExists() {
    return ((this.page * 20) < 100);
  }

  prevPageExists() {
    return this.page !== 1;
  }

}
