import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {AppState, selectGeocodeAll} from '../../../../store';
import {ILatLng} from '../interfaces';
import {GeoCode} from '../../../../core/interfaces';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RestaurantsComponent {

  geocode$: Observable<GeoCode>;
  topTen: boolean;
  location: ILatLng;

  constructor(private store: Store<AppState>) {
    this.geocode$ = this.store.select(selectGeocodeAll);
    this.topTen = true;
  }

  changeList() {
    this.topTen = !this.topTen;
  }

  handleLocationUpdates(event: ILatLng) {
    this.location = {lat: event.lat, lon: event.lon};
  }
}
