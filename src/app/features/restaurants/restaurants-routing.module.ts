import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {RestaurantsComponent, RestaurantDetailsComponent} from './page';
import {RestaurantResolver} from '../../shared';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild([
      {
        path: '', component: RestaurantsComponent
      },
      {
        path: ':id', component: RestaurantDetailsComponent,
        resolve: {
          restaurant: RestaurantResolver
        }
      }
    ])
  ],
  declarations: [],
  providers: [],
  exports: [
    RouterModule
  ]
})
export class RestaurantsRoutingModule {
}
