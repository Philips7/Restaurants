import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';

import {RestaurantsRoutingModule} from './restaurants-routing.module';
import {CommonService, LocationService, RestaurantService} from './services';
import {
  RestaurantDetailsComponent,
  RestaurantsComponent,
  RestaurantsListComponent
} from './page';
import {SharedModule, RestaurantResolver} from '../../shared';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    RestaurantsRoutingModule,
    SharedModule
  ],
  declarations: [
    RestaurantDetailsComponent,
    RestaurantsComponent,
    RestaurantsListComponent
  ],
  providers: [
    CommonService,
    LocationService,
    RestaurantService,
    RestaurantResolver
  ]
})

export class RestaurantsModule {
}
