import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

import {ICity, ICollection, ICuisines, IEstablishment} from './interfaces';
import {IGeocode} from '../../../../core';
import {environment} from '@env/environment';

type PARAMS = ICity | ICollection | ICuisines | IEstablishment | IGeocode;

@Injectable()
export class CommonService {
  categoriesPath: string;
  citiesPath: string;
  collectionsPath: string;
  cuisinesPath: string;
  establishmentsPath: string;
  geocodePath: string;

  constructor(private http: HttpClient) {
    this.categoriesPath = '/categories';
    this.citiesPath = '/cities';
    this.collectionsPath = '/collections';
    this.cuisinesPath = '/cuisines';
    this.establishmentsPath = '/establishments';
    this.geocodePath = '/geocode';
  }

  getCategories() {

    return this.http.get(`${environment.zomato.url + this.categoriesPath}`)
      .map((response: Response) => response['categories']);
  }

  getCities(citiesRequest: ICity) {
    const params = this.setParams(citiesRequest);

    return this.http.get(`${environment.zomato.url + this.citiesPath}`,
      {params})
      .map((response: Response) => response['location_suggestions']);
  }

  getCollections(collectionRequest: ICollection) {
    const params = this.setParams(collectionRequest);

    return this.http.get(`${environment.zomato.url + this.collectionsPath}`,
      {params})
      .map((response: Response) => response['collections']);

  }

  getCuisines(cuisinesRequest: ICuisines) {
    const params = this.setParams(cuisinesRequest);

    return this.http.get(`${environment.zomato.url + this.cuisinesPath}`,
      {params})
      .map((response: Response) => response['cuisines']);
  }

  getEstablishments(establishmentRequest: IEstablishment) {
    const params = this.setParams(establishmentRequest);

    return this.http.get(`${environment.zomato.url + this.establishmentsPath}`,
      {params})
      .map((response: Response) => response['establishments']);
  }

  getGeocode(geocodeRequest: IGeocode) {
    const params = this.setParams(geocodeRequest);

    return this.http.get(`${environment.zomato.url + this.geocodePath}`, {params});
  }

  private setParams(httpParams: PARAMS) {
    return Object.keys(httpParams).reduce((previousValue, currentValue, index, array) => {
      const value = httpParams[`${currentValue}`];
      return previousValue.append(currentValue, value);
    }, new HttpParams());
  }

}
