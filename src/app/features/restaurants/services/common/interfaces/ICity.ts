export interface ICity {
  q?: string;
  lat?: number;
  lon?: number;
  city_ids?: string;
  count?: number;
}
