export interface ILocations {
  city_id: number;
  city_name: string;
  country_id: number;
  country_name: string;
  entity_id: number;
  entity_type: string;
  latitude: number;
  longitude: number;
  title: string;
}
