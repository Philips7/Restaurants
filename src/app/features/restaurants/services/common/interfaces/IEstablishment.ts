export interface IEstablishment {
  'city_id'?: number;
  lat?: number;
  lon?: number;
}
