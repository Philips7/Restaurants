import {IUserRatings} from './IUserRatings';

export interface INearbyRestaurants {
    R: {
      res_id: number;
    };
    average_cost_for_two: number;
    currency: string;
    deeplink: string;
    location: {
      address: string,
      latitude: number,
      longitude: number
    };
    menu_url: string;
    name: string;
    photos_url: string;
    url: string;
    user_rating: IUserRatings;
}
