export interface ICollection {
  'city_id'?: number;
  lat?: number;
  lon?: number;
  count?: number;
}
