export interface IUserRatings {
  aggregate_rating: number;
  rating_text: string;
  votes: string;
}
