import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import {CommonService} from './common.service';
import {ICity, ICollection, ICuisines, IEstablishment, IGeocode} from './interfaces';
import {environment} from '@env/environment';

describe('CommonService', () => {

  let service: CommonService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        CommonService
      ]
    });
    service = TestBed.get(CommonService);
    httpMock = TestBed.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should handle getting categories', () => {
    const categories = {
      'categories': [
        {
          'categories': {
            'id': 1,
            'name': 'Delivery'
          }
        },
        {
          'categories': {
            'id': 2,
            'name': 'Dine-out'
          }
        },
        {
          'categories': {
            'id': 3,
            'name': 'Nightlife'
          }
        },
        {
          'categories': {
            'id': 4,
            'name': 'Catching-up'
          }
        },
        {
          'categories': {
            'id': 5,
            'name': 'Takeaway'
          }
        },
        {
          'categories': {
            'id': 6,
            'name': 'Cafes'
          }
        },
        {
          'categories': {
            'id': 7,
            'name': 'Daily Menus'
          }
        },
        {
          'categories': {
            'id': 8,
            'name': 'Breakfast'
          }
        },
        {
          'categories': {
            'id': 9,
            'name': 'Lunch'
          }
        },
        {
          'categories': {
            'id': 10,
            'name': 'Dinner'
          }
        },
        {
          'categories': {
            'id': 11,
            'name': 'Pubs & Bars'
          }
        },
        {
          'categories': {
            'id': 13,
            'name': 'Pocket Friendly Delivery'
          }
        },
        {
          'categories': {
            'id': 14,
            'name': 'Clubs & Lounges'
          }
        }
      ]
    };

    service.getCategories().subscribe(response => {
      expect(response).toEqual(categories.categories);
    });

    const req = httpMock.expectOne(`${environment.zomato.url}/categories`);
    expect(req.request.method).toEqual('GET');

    req.flush(categories);
  });

  it('should handle getting cities', () => {
    const cities: ICity = {
      lat: 50.04132,
      lon: 21.99901,
    };
    const citiesResponse = {
      'location_suggestions': [
        {
          'id': 267,
          'name': 'Rzeszów',
          'country_id': 163,
          'country_name': 'Poland',
          'should_experiment_with': 0,
          'discovery_enabled': 0,
          'has_new_ad_format': 0,
          'is_state': 0,
          'state_id': 0,
          'state_name': '',
          'state_code': ''
        }
      ],
      'status': 'success',
      'has_more': 0,
      'has_total': 0
    };

    service.getCities(cities).subscribe((response => {
        expect(response).toEqual(citiesResponse.location_suggestions);
      })
    );

    const req = httpMock
      .expectOne(`${environment.zomato.url}/cities?lat=50.04132&lon=21.99901`);
    expect(req.request.method).toEqual('GET');

    req.flush(citiesResponse);
  });

  it('should handle getting categories', () => {
    const collection: ICollection = {
      'city_id': 1,
      count: 1
    };
    const collectionsResponse = {
      'collections': [
        {
          'collection': {
            'collection_id': 1,
            'res_count': 30,
            'image_url': 'https://b.zmtcdn.com/data/collections/e140962ec7eecbb851155fe0bb0cd28c_1501223786.jpg',
            'url': 'https://www.zomato.com/rzeszów/top-restaurants?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'title': 'Trending this week',
            'description': 'The most popular restaurants in town this week',
            'share_url': 'http://www.zoma.to/c-1/1'
          }
        },
        {
          'collection': {
            'collection_id': 274852,
            'res_count': 71,
            'image_url': 'https://b.zmtcdn.com/data/collections/bd21c655d3235173ce84b32293fec15b_1506085178.jpg',
            'url': 'https://www.zomato.com/rzeszów/great-food-no-bull?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'title': 'Great food, no bull',
            'description': 'The hunt for the highest-rated restaurants in your city ends here',
            'share_url': 'http://www.zoma.to/c-1/274852'
          }
        },
        {
          'collection': {
            'collection_id': 29,
            'res_count': 32,
            'image_url': 'https://b.zmtcdn.com/data/collections/b8499c7a6b74ddf01497ac8afc86d2e2_1476701306.jpg',
            'url': 'https://www.zomato.com/rzeszów/best-new-restaurants?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'title': 'Newly Opened',
            'description': 'The best new places in town',
            'share_url': 'http://www.zoma.to/c-1/29'
          }
        }
      ]
    };

    service.getCollections(collection).subscribe((response) => {
      expect(response).toEqual(collectionsResponse.collections);
    });

    const req = httpMock
      .expectOne(`${environment.zomato.url}/collections?city_id=1&count=1`);
    expect(req.request.method).toEqual('GET');

    req.flush(collectionsResponse);
  });

  it('should handle getting cuisines', () => {
    const cuisine: ICuisines = {
      'city_id': 267
    };
    const cuisinesResponse = {
      'cuisines':
        [
          {
            'cuisine': {
              'cuisine_id': 1,
              'cuisine_name': 'American'
            }
          },
          {
            'cuisine': {
              'cuisine_id': 4,
              'cuisine_name': 'Arabian'
            }
          },
          {
            'cuisine': {
              'cuisine_id': 3,
              'cuisine_name': 'Asian'
            }
          }
        ]

    };

    service.getCuisines(cuisine).subscribe((response) => {
      expect(response).toEqual(cuisinesResponse.cuisines);
    });

    const req = httpMock.expectOne(`${environment.zomato.url}/cuisines?city_id=267`);
    expect(req.request.method).toEqual('GET');

    req.flush(cuisinesResponse);
  });

  it('should handle getting establishments', () => {
    const establishment: IEstablishment = {
      'city_id': 267,
      lat: 50.04132,
      lon: 21.99901
    };
    const establishmentsResponse = {
      'establishments': [
        {
          'establishment': {
            'id': 16,
            'name': 'Casual Dining'
          }
        },
        {
          'establishment': {
            'id': 23,
            'name': 'Dessert Parlour'
          }
        },
        {
          'establishment': {
            'id': 7,
            'name': 'Bar'
          }
        },
        {
          'establishment': {
            'id': 6,
            'name': 'Pub'
          }
        },
        {
          'establishment': {
            'id': 8,
            'name': 'Club'
          }
        },
        {
          'establishment': {
            'id': 1,
            'name': 'Café'
          }
        },
        {
          'establishment': {
            'id': 91,
            'name': 'Bistro'
          }
        },
        {
          'establishment': {
            'id': 211,
            'name': 'Canteen'
          }
        },
        {
          'establishment': {
            'id': 20,
            'name': 'Food Court'
          }
        },
        {
          'establishment': {
            'id': 21,
            'name': 'Quick Bites'
          }
        },
        {
          'establishment': {
            'id': 161,
            'name': 'Microbrewery'
          }
        },
        {
          'establishment': {
            'id': 278,
            'name': 'Wine Bar'
          }
        },
        {
          'establishment': {
            'id': 111,
            'name': 'Tea Room'
          }
        },
        {
          'establishment': {
            'id': 31,
            'name': 'Bakery'
          }
        }
      ]
    };

    service.getEstablishments(establishment).subscribe((response) => {
      expect(response).toEqual(establishmentsResponse.establishments);
    });

    const req = httpMock
      .expectOne(`${environment.zomato.url}/establishments?city_id=267&lat=50.04132&lon=21.99901`);
    expect(req.request.method).toEqual('GET');

    req.flush(establishmentsResponse);
  });

  it('should handle getting geocode', () => {
    const geocode: IGeocode = {
      lat: 50.04132,
      lon: 21.99901
    };
    const geocodeResponse = {
      'location': {
        'entity_type': 'subzone',
        'entity_id': 97456,
        'title': 'Rzeszów',
        'latitude': '50.0499990000',
        'longitude': '22.0000000000',
        'city_id': 267,
        'city_name': 'Rzeszów',
        'country_id': 163,
        'country_name': 'Poland'
      },
      'popularity': {
        'popularity': '2.97',
        'nightlife_index': '2.25',
        'nearby_res': [
          '16533762',
          '18241612',
          '16533772',
          '16533774',
          '16534018',
          '16534081',
          '16533824',
          '16533798',
          '16533844'
        ],
        'top_cuisines': [
          'Polish',
          'Drinks Only',
          'Pizza',
          'Cafe',
          'Finger Food'
        ],
        'popularity_res': '100',
        'nightlife_res': '10',
        'subzone': 'Rzeszów',
        'subzone_id': 97456,
        'city': 'Rzeszów'
      },
      'link': 'https://www.zomato.com/rzeszów/rzeszów-restaurants',
      'nearby_restaurants': [
        {
          'restaurant': {
            'R': {
              'res_id': 16533762
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '16533762',
            'name': 'Cukiernia Wiedeńska - Hotel Ambasadorski',
            'url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 13-14, Rzeszów, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0372540000',
              'longitude': '22.0056600000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Desserts, Grill, Cafe',
            'average_cost_for_two': 35,
            'price_range': 1,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.3',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '14'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/16533762',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/cukiernia-wiedeńska-hotel-ambasadorski-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        },
        {
          'restaurant': {
            'R': {
              'res_id': 18241612
            },
            'apikey': '36b64a2a607a69ff0b7cd362727d9008',
            'id': '18241612',
            'name': 'Radość',
            'url': 'https://www.zomato.com/rzeszów/rado-rzeszów?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1',
            'location': {
              'address': 'Rynek 24, Rzeszów',
              'locality': 'Rzeszów',
              'city': 'Rzeszów',
              'city_id': 267,
              'latitude': '50.0378680000',
              'longitude': '22.0044360000',
              'zipcode': '',
              'country_id': 163,
              'locality_verbose': 'Rzeszów, Rzeszów'
            },
            'switch_to_order_menu': 0,
            'cuisines': 'Burger, Polish, European',
            'average_cost_for_two': 75,
            'price_range': 2,
            'currency': 'zł',
            'offers': [],
            'thumb': '',
            'user_rating': {
              'aggregate_rating': '3.2',
              'rating_text': 'Average',
              'rating_color': 'CDD614',
              'votes': '6'
            },
            'photos_url': 'https://www.zomato.com/rzeszów/rado-rzeszów/photos?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1#tabtop',
            'menu_url': 'https://www.zomato.com/rzeszów/rado-rzeszów/menu?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1&openSwipeBox=menu&showMinimal=1#tabtop',
            'featured_image': '',
            'has_online_delivery': 0,
            'is_delivering_now': 0,
            'deeplink': 'zomato://restaurant/18241612',
            'has_table_booking': 0,
            'events_url': 'https://www.zomato.com/rzeszów/rado-rzeszów/events#tabtop?utm_source=api_basic_user&utm_medium=api&utm_campaign=v2.1'
          }
        }
      ]

    };

    service.getGeocode(geocode).subscribe(response => {
      expect(response).toEqual(geocodeResponse);
    });

    const req = httpMock
      .expectOne(`${environment.zomato.url}/geocode?lat=50.04132&lon=21.99901`);
    expect(req.request.method).toEqual('GET');

    req.flush(geocodeResponse);
  });
});
