import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import { environment } from '@env/environment';
import {ILocation, ILocationDetails} from './interfaces';

@Injectable()
export class LocationService {
  headerKey = 'user-key';
  apiHeader = new HttpHeaders().set(this.headerKey, environment.zomato.apiKey);

  locationsPath = '/locations';
  locationDetailsPath = '/location_details';

  constructor(private http: HttpClient) {
  }

  getLocation(locationRequest: ILocation) {

    // Setting Query Params
    let params = new HttpParams();
    params = params.append('lat', `${locationRequest.lat}`);
    params = params.append('lon', `${locationRequest.lon}`);

    // API Call
    return this.http.get(`${environment.zomato.url + this.locationsPath}`,
      {headers: this.apiHeader, params: params})
      .map((response: Response) => response);
  }

  getLocationDetails(locationDetailsRequest: ILocationDetails) {

    // Setting Query Params
    let params = new HttpParams();
    params = params.append('entity_id', `${locationDetailsRequest.entity_id}`);
    params = params.append('entity_type', `${locationDetailsRequest.entity_type}`);

    // API Call
    return this.http.get(`${environment.zomato.url + this.locationDetailsPath}`,
      {headers: this.apiHeader, params: params})
      .map((response: Response) => response);
  }


}
