export interface ILocation {
  query: string;
  lat?: number;
  lon?: number;
  count?: number;
}
