export interface ISearch {
  entity_id?: number;
  q?: string;
  start?: number;
  count?: number;
  lat: number;
  lon: number;
  radius?: number;
  sort?: string;
}
