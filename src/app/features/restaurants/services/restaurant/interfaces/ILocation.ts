export interface ILocation {
  address: string;
  city: string;
  latitude: string;
  longitude: string;
  zipcode: string;
}
