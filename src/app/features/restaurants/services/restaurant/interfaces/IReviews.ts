export interface IReviews {
  res_id: number;
  start?: number;
  count?: number;
}
