import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {IDailyMenu, IRestaurant, IReviews} from './interfaces/';


@Injectable()
export class RestaurantService {
  dailyMenuPath = '/dailymenu';
  restaurant = '/restaurant';
  reviews = '/reviews';
  search = '/search';

  constructor(private http: HttpClient) {
  }

  getDailyMenu(dailyMenuRequest: IDailyMenu) {

  }

  getRestaurant(restaurantRequest: IRestaurant) {

  }

  getReviews(reviewsRequest: IReviews) {

  }

  // getSearch(searchRequest: ISearch) {
  //
  // }

}
