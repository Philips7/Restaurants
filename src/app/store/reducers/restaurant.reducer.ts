import {createSelector} from '@ngrx/store';

import {
  GET_RESTAURANT,
  GET_RESTAURANT__ERROR,
  GET_RESTAURANT__SUCCESS,
  RESTAURANT_ALL
} from '../actions';
import {AppState} from '../interfaces';
import {Restaurant} from '../../core';

type Action = RESTAURANT_ALL;

export const selectRestaurant = (state: AppState) => state.restaurant;
export const selectRestaurantAll = createSelector(selectRestaurant, (state: Restaurant) => state);
export const selectRestaurantLoading = createSelector(selectRestaurant, (state: Restaurant) => state.rest!.loading);

const defaultRestaurantState: Restaurant = new Restaurant(null);

export function restaurantReducer(state: Restaurant = defaultRestaurantState, action: Action) {
  switch (action.type) {
    case GET_RESTAURANT:
      return {...defaultRestaurantState, error: null, loading: true};
    case GET_RESTAURANT__SUCCESS:
      return {...state, ...action.payload, loading: false, res_id: null, error: null};
    case GET_RESTAURANT__ERROR:
      return {...state, error: action.payload, loading: false};
    default:
      return state;
  }
}
