import {createSelector} from '@ngrx/store';

import {GET_USER, AUTHENTICATED, NOT_AUTHENTICATED, GOOGLE_LOGIN, AUTH_ERROR, LOGOUT, All} from '../actions';
import {AppState} from '../interfaces';
import {IUser, User} from '../../core';

export type Action = All;

const defaultUser = new User(null, 'GUEST');

export const selectUser = (state: AppState) => state.user;
export const selectUserAll = createSelector(selectUser, (state: IUser) => state);

export function userReducer(state: User = defaultUser, action: Action) {

  switch (action.type) {

    case GET_USER:
      return {...state, loading: true};

    case AUTHENTICATED:
      return {...state, ...action.payload, loading: false};

    case NOT_AUTHENTICATED:
      return defaultUser;

    case GOOGLE_LOGIN:
      return {...state, loading: true};

    case AUTH_ERROR:
      return {...state, error: action.payload, loading: false};

    case LOGOUT:
      return {...state, loading: true};
    default:
      return state;
  }
}
