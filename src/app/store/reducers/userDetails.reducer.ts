import {createSelector} from '@ngrx/store';

import {UserDetails} from '../../core';
import {
  GET_USER_DETAILS,
  GET_USER_DETAILS__ERROR,
  GET_USER_DETAILS__SUCCESS,
  POST_USER_ADDRESS,
  POST_USER_ADDRESS__ERROR,
  POST_USER_ADDRESS__SUCCESS,
  POST_USER_DATA,
  POST_USER_DATA__ERROR,
  POST_USER_DATA__SUCCESS,
  REMOVE_USER_ADDRESS,
  REMOVE_USER_ADDRESS__ERROR,
  REMOVE_USER_ADDRESS__SUCCESS,
  UPDATE_USER_ADDRESS_WITH_NAME,
  UPDATE_USER_ADDRESS_WITH_NAME__ERROR,
  UPDATE_USER_ADDRESS_WITH_NAME__SUCCESS,
  USER_DETAILS_ALL
} from '../actions';
import {AppState} from '../interfaces';

type Action = USER_DETAILS_ALL;

const userDetailsDefaultState: UserDetails = new UserDetails(null);

export const selectUserDetails = (state: AppState) => state.userDetails;
export const selectUserDetailsAll = createSelector(selectUserDetails, (state: UserDetails) => state);
export const selectUserDetailsPlaces = createSelector(selectUserDetails, (state: UserDetails) => state!.address);

export function userDetailsReducer(state: UserDetails = userDetailsDefaultState,
                                   action: Action) {
  switch (action.type) {

    case GET_USER_DETAILS:
      return {...state, error: null, loading: true, loadingUser: true};

    case GET_USER_DETAILS__SUCCESS:
      return {...state, ...action.payload, error: null, loading: false, loadingUser: false};

    case GET_USER_DETAILS__ERROR:
      return {...state, error: action.payload, loading: false, loadingUser: false};

    case POST_USER_DATA:
      return {...state, error: null, loadingUser: true};

    case POST_USER_DATA__SUCCESS:
      return {...state, error: null, loadingUser: false};

    case POST_USER_DATA__ERROR:
      return {...state, error: action.payload, loadingUser: false};

    case POST_USER_ADDRESS:
      return {...state, error: null, loading: true};

    case POST_USER_ADDRESS__SUCCESS:
      return {...state, error: null, loading: false};

    case POST_USER_ADDRESS__ERROR:
      return {...state, error: action.payload, loading: false};

    case REMOVE_USER_ADDRESS:
      return {...state, removeName: action.payload, error: null, loading: true};

    case REMOVE_USER_ADDRESS__SUCCESS:
      return {...state, error: null, loading: false};

    case REMOVE_USER_ADDRESS__ERROR:
      return {...state, error: action.payload, loading: false};

    case UPDATE_USER_ADDRESS_WITH_NAME:
      return {...state, ...action.payload, error: null, loading: true};

    case UPDATE_USER_ADDRESS_WITH_NAME__SUCCESS:
      return {...state, error: null, loading: false};

    case UPDATE_USER_ADDRESS_WITH_NAME__ERROR:
      return {...state, error: action.payload, loading: false};

    default:
      return state;
  }
}
