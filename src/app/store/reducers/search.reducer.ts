import {createSelector} from '@ngrx/store';

import {GET_SEARCH, GET_SEARCH__ERROR, GET_SEARCH__SUCCESS, Search_ALL} from '../actions';
import {AppState} from '../interfaces';
import {Search} from '../../core';

type Action = Search_ALL;

export const selectSearch = (state: AppState) => state.search;
export const selectSearchAll = createSelector(selectSearch, (state: Search) => state);
export const selectSearchResultsFound = createSelector(selectSearch, (state: Search) => state.results_found);
export const selectSearchResultsShown = createSelector(selectSearch, (state: Search) => state.results_shown);
export const selectSearchResultsStart = createSelector(selectSearch, (state: Search) => state.results_start);

const defaultSearchState = new Search(null, null, null, null);

export function searchReducer(state: Search = defaultSearchState, action: Action) {
  switch (action.type) {
    case GET_SEARCH:
      return {...state, ...action.payload, loading: true};
    case GET_SEARCH__SUCCESS:
      return {...state, ...action.payload, loading: false};
    case GET_SEARCH__ERROR:
      return {...state, error: action.payload, loading: false};
    default:
      return state;
  }
}
