import {createSelector} from '@ngrx/store';

import {GET_GEOCODE,
  GET_GEOCODE__SUCCESS,
  GET_GEOCODE__ERROR,
  GEOCODE_ALL} from '../actions';
import {AppState} from '../interfaces';
import {GeoCode} from '../../core';

type Action = GEOCODE_ALL;

export const selectGeocode = (state: AppState) => state.geocode;
export const selectGeocodeAll = createSelector(selectGeocode, (state: GeoCode) => state);

const defaultGeocodeState = new GeoCode(null, null, null, false);

export function geocodeReducer(state: GeoCode = defaultGeocodeState, action: Action) {
  switch (action.type) {
    case GET_GEOCODE:
      return {...state, ...action.payload, loading: true};
    case GET_GEOCODE__SUCCESS:
      return {...state, ...action.payload, loading: false};
    case GET_GEOCODE__ERROR:
      return {...state, error: action.payload, loading: false};
    default:
      return state;
  }
}
