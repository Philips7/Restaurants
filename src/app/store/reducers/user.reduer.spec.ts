import {userReducer} from './user.reducer';
import {User} from '../../features';
import {
  Authenticated,
  AuthError, GetUser,
  GoogleLogin,
  Logout,
  NotAuthenticated
} from '../actions';

describe('UserReducer', () => {

  it('should handle GET_USER', () => {
    const defaultUser = new User(null, 'GUEST');

    expect(
      userReducer(undefined, new GetUser())
    )
      .toEqual({...defaultUser, loading: true});
  });

  it('should handle AUTHENTICATED', () => {
    const defaultUser = new User(null, 'GUEST');
    const payload = {
      uid: '1234567',
      displayName: 'Dominik'
    };

    expect(
      userReducer(defaultUser, new Authenticated(payload))
    )
      .toEqual({...defaultUser, ...payload, loading: false});
  });

  it('should handle NOT_AUTHENTICATED ', () => {
    const defaultUser = new User(null, 'GUEST');

    expect(
      userReducer(defaultUser, new NotAuthenticated())
    )
      .toEqual(defaultUser);
  });

  it('should handle GOOGLE_LOGIN', () => {
    const randomUser = new User('1234567', 'Dominik');

    expect(
      userReducer(randomUser, new GoogleLogin())
    )
      .toEqual({...randomUser, loading: true});
  });

  it('should handle AUTH_ERROR', () => {
    const defaultUser = new User(null, 'GUEST');
    const payload = 'Access Forbidden';

    expect(
      userReducer(defaultUser, new AuthError(payload))
    )
      .toEqual({...defaultUser, error: payload, loading: false});
  });

  it('should handle LOGOUT', () => {
    const randomUser = new User('1234567', 'Dominik');

    expect(
      userReducer(randomUser, new Logout())
    )
      .toEqual({...randomUser, loading: true});
  });

});
