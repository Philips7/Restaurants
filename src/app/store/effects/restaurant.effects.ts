import {Injectable} from '@angular/core';
import {Actions, Effect} from '@ngrx/effects';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import {GET_RESTAURANT, GetRestaurant, GetRestaurantError, GetRestaurantSuccess, RESTAURANT_ALL} from '../actions';
import {IRestaurantResponse, Restaurant} from '../../core';
import {environment} from '@env/environment';

type Action = RESTAURANT_ALL;

@Injectable()
export class RestaurantEffects {

  @Effect()
  getRestaurant: Observable<Action> = this.actions.ofType(GET_RESTAURANT)
    .map((action: GetRestaurant) => action.payload)
    .switchMap(payload => {
      const restaurantPath = '/restaurant';
      let params = new HttpParams();
      params = params.append('res_id', `${payload.res_id}`);

      return this.http.get(environment.zomato.url + restaurantPath, {params: params})
        .map((response: IRestaurantResponse) => {
          const restaurantResponse = new Restaurant(response);
          return new GetRestaurantSuccess(restaurantResponse);
        })
        .catch((err: HttpErrorResponse) => of(new GetRestaurantError(err.message)));
    })
    .catch((err: HttpErrorResponse) => of(new GetRestaurantError(err.message)));

  constructor(private actions: Actions, private http: HttpClient) {
  }
}
