import {Actions, Effect} from '@ngrx/effects';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {HttpClient, HttpErrorResponse, HttpParams} from '@angular/common/http';

import {GEOCODE_ALL,
  GeoGeocodeSuccess,
  GET_GEOCODE, GetGeocode,
  GetGeocodeError} from '../actions';
import {GeoCode, IGeocodeResponse} from '../../core';
import {INearbyRestaurants} from '../../features';
import {environment} from '@env/environment';

type Action = GEOCODE_ALL;

@Injectable()
export class GeocodeEffects {

  @Effect()
  getGeocode: Observable<Action> = this.actions.ofType(GET_GEOCODE)
    .map((action: GetGeocode) => action.payload)
    .switchMap(payload => {
      const geocodePath = '/geocode';
      let params = new HttpParams();
      params = params.append('lat', `${payload.lat}`);
      params = params.append('lon', `${payload.lon}`);

      return this.http.get(environment.zomato.url + geocodePath, {params});

    })
    .map((response: IGeocodeResponse) => {
      const restaurants: INearbyRestaurants[] = [];
      response.nearby_restaurants!.forEach((rest) => {
        restaurants.push(rest['restaurant']);
      });

      const geocodeResponse = new GeoCode(response.link,
        response.location, restaurants);
      return new GeoGeocodeSuccess(geocodeResponse);
    })
    .catch((err: HttpErrorResponse) => of(new GetGeocodeError(err.message)));

  constructor(private actions: Actions, private http: HttpClient) {
  }

}
