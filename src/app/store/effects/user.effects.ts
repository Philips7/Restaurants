import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Actions, Effect} from '@ngrx/effects';
import {AngularFireAuth} from 'angularfire2/auth';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {fromPromise} from 'rxjs/observable/fromPromise';
import * as firebase from 'firebase';

import {
  All,
  Authenticated,
  AuthError,
  GET_USER,
  GetUser,
  GOOGLE_LOGIN,
  GoogleLogin,
  LOGOUT,
  NotAuthenticated
} from '../actions';
import {User} from '../../core';
import {Router} from '@angular/router';

type Action = All;

@Injectable()
export class UserEffects {

  @Effect()
  getUser: Observable<Action> = this.actions.ofType(GET_USER)
    .switchMap(() => this.afAuth.authState)
    .map(authData => {
      if (authData) {
        const user = new User(authData.uid, authData.displayName!);
        return new Authenticated(user);
      } else {
        return new NotAuthenticated();
      }
    })
    .catch(err => of(new AuthError()));

  @Effect()
  login: Observable<Action> = this.actions.ofType(GOOGLE_LOGIN)
    .map((action: GoogleLogin) => action)
    .switchMap(() => {
      return fromPromise(this.googleLogin());
    })
    .map(() => {
      return new GetUser();
    })
    .catch((err: HttpErrorResponse) => {
      return of(new AuthError(err.message));
    });

  @Effect()
  logout: Observable<Action> = this.actions.ofType(LOGOUT)
    .switchMap(() => of(this.afAuth.auth.signOut()))
    .map(() => {
    this.router.navigateByUrl('restaurants');
    return new NotAuthenticated();
    })
    .catch((err: HttpErrorResponse) => of(new AuthError(err.message)));

  constructor(private actions: Actions, private afAuth: AngularFireAuth, private router: Router) {
  }

  private googleLogin(): Promise<void> {
    const provider = new firebase.auth.GoogleAuthProvider();
    return this.afAuth.auth.signInWithPopup(provider);
  }

}
