import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Actions, Effect} from '@ngrx/effects';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';

import {GET_SEARCH, GetSearch, GetSearchError, GetSearchSuccess, Search_ALL} from '../actions';
import {INearbyRestaurants} from '../../features';
import {ISearchResponse, Search} from '../../core';
import {environment} from '@env/environment';

type Action = Search_ALL;

@Injectable()
export class SearchEffects {

  @Effect()
  getSearch: Observable<Action> = this.actions.ofType(GET_SEARCH)
    .map((action: GetSearch) => action.payload)
    .switchMap(payload => {
      const headerKey = 'user-key';
      const searchPath = '/search';
      const apiHeader = new HttpHeaders().set(headerKey, environment.zomato.apiKey);
      let params = new HttpParams();
      params = params.append('lat', `${payload.lat}`);
      params = params.append('lon', `${payload.lon}`);
      params = params.append('start', `${payload.start}`);

      return this.http.get(environment.zomato.url + searchPath,
        {headers: apiHeader, params});

    })
    .map((response: ISearchResponse) => {
      const restaurants: INearbyRestaurants[] = [];
      response.restaurants!.forEach((rest) => {
        restaurants.push(rest['restaurant']);
      });

      const searchResponse = new Search(response.results_found, response.results_start,
        response.results_shown, restaurants);
      return new GetSearchSuccess(searchResponse);
    })
    .catch((err: HttpErrorResponse) => of(new GetSearchError(err.message)));

  constructor(private actions: Actions, private http: HttpClient) {
  }
}
