import {Injectable} from '@angular/core';
import {HttpErrorResponse} from '@angular/common/http';
import {Actions, Effect} from '@ngrx/effects';
import {Store} from '@ngrx/store';
import {AngularFirestore} from 'angularfire2/firestore';
import {Observable} from 'rxjs/Observable';
import {zip} from 'rxjs/observable/zip';
import {of} from 'rxjs/observable/of';
import {fromPromise} from 'rxjs/observable/fromPromise';

import {Address, IUserDetails, UserDetails} from '../../core';
import {
  GET_USER_DETAILS,
  GetUserDetails,
  GetUserDetailsError,
  GetUserDetailsSuccess,
  POST_USER_ADDRESS,
  POST_USER_DATA,
  PostUserAddress,
  PostUserAddressError,
  PostUserAddressSuccess,
  PostUserData,
  REMOVE_USER_ADDRESS,
  RemoveUserAddress,
  RemoveUserAddressError,
  RemoveUserAddressSuccess,
  UPDATE_USER_ADDRESS,
  UPDATE_USER_ADDRESS_WITH_NAME,
  UpdateUserAddress,
  UpdateUserAddressError,
  UpdateUserAddressSuccess,
  UpdateUserAddressWithName,
  UpdateUserAddressWithNameError,
  UpdateUserAddressWithNameSuccess,
  USER_DETAILS_ALL
} from '../actions';
import {AppState, IUpdateAddressWithName} from '../interfaces';
import {environment} from '@env/environment';

type Action = USER_DETAILS_ALL;

@Injectable()
export class UserDetailsEffects {

  @Effect()
  getUserDetails: Observable<Action> = this.actions.ofType(GET_USER_DETAILS)
    .switchMap(() => {
      const uid = this.getUid();

      const places = this.afs.collection(`users/${uid}/places`).valueChanges()
        .catch((err: HttpErrorResponse) => of(new GetUserDetailsError(err.message)));
      const userData = this.afs.doc(`users/${uid}`).valueChanges()
        .catch((err: HttpErrorResponse) => of(new GetUserDetailsError(err.message)));

      return zip(
        places,
        userData
      );
    })
    .map((resp: any) => {
      const userData: IUserDetails = {
        name: resp[1].name,
        age: resp[1].age,
        sex: resp[1].sex,
        address: <Address[]>resp[0]
      };
      const userDetails: UserDetails = new UserDetails(userData);

      return new GetUserDetailsSuccess(userDetails);
    })
    .catch((err: HttpErrorResponse) => of(new GetUserDetailsError(err.message)));

  @Effect()
  postUserAddress: Observable<Action> = this.actions.ofType(POST_USER_ADDRESS)
    .map((action: PostUserAddress) => action.payload)
    .switchMap((payload) => {
      const uid = this.getUid();

      return fromPromise(this.afs.doc(`users/${uid}/places/${payload.name}`).set({
        name: payload.name, street: payload.street, city: payload.city,
        zipCode: payload.zipCode, state: payload.state
      }))
        .catch((err: HttpErrorResponse) => of(new PostUserAddressError(err.message)));
    })
    .map(() => new PostUserAddressSuccess())
    .do(() => this.store.dispatch(new GetUserDetails()))
    .catch((err: HttpErrorResponse) => of(new PostUserAddressError(err.message)));

  @Effect()
  updateUserAddress: Observable<Action> = this.actions.ofType(UPDATE_USER_ADDRESS)
    .map((action: UpdateUserAddress) => action.payload)
    .switchMap((payload) => {
      const uid = this.getUid();

      return this.afs.doc(`users/${uid}/places/${payload.name}`).update({
        name: payload.name, street: payload.street, city: payload.city,
        zipCode: payload.zipCode, state: payload.state
      })
        .catch((err: HttpErrorResponse) => of(new PostUserAddressError(err.message)));
    })
    .map(() => new UpdateUserAddressSuccess())
    .do(() => this.store.dispatch(new GetUserDetails()))
    .catch((err: HttpErrorResponse) => of(new UpdateUserAddressError(err.message)));

  @Effect()
  removeUserAddress: Observable<Action> = this.actions.ofType(REMOVE_USER_ADDRESS)
    .map((action: RemoveUserAddress) => action.payload)
    .switchMap((payload) => {
      const uid = this.getUid();

      return this.afs.doc(`users/${uid}/places/${payload}`).delete()
        .catch((err: HttpErrorResponse) => of(new RemoveUserAddressError(err.message)));
    })
    .map((x) => new RemoveUserAddressSuccess())
    .do(() => this.store.dispatch(new GetUserDetails()))
    .catch((err: HttpErrorResponse) => of(new RemoveUserAddressError(err.message)));

  @Effect()
  postUserData: Observable<Action> = this.actions.ofType(POST_USER_DATA)
    .map((action: PostUserData) => action.payload)
    .switchMap((payload: UserDetails) => {
      const uid = this.getUid();

      return this.afs.doc(`users/${uid}`)
        .set({name: payload.user!.name, age: payload.user!.age, sex: payload.user!.sex})
        .catch((err: HttpErrorResponse) => of(new PostUserAddressError(err.message)));
    })
    .map(() => new PostUserAddressSuccess())
    .catch((err: HttpErrorResponse) => of(new PostUserAddressError(err.message)));

  @Effect()
  updateUserAddressWithName: Observable<Action> = this.actions.ofType(UPDATE_USER_ADDRESS_WITH_NAME)
    .map((action: UpdateUserAddressWithName) => action.payload)
    .switchMap((payload: IUpdateAddressWithName) => {
      const uid = this.getUid();

      const deleteAddress = fromPromise(this.afs.doc(`users/${uid}/places/${payload.deletedName}`)
        .delete())
        .catch((err: HttpErrorResponse) => of(new UpdateUserAddressWithNameError(err.message)));

      const createAddress = fromPromise(this.afs.doc(`users/${uid}/places/${payload.singleAddress!.name}`).set({
        name: payload.singleAddress!.name,
        street: payload.singleAddress!.street,
        city: payload.singleAddress!.city,
        zipCode: payload.singleAddress!.zipCode,
        state: payload.singleAddress!.state
      }))
        .catch((err: HttpErrorResponse) => of(new UpdateUserAddressWithNameError(err.message)));

      return zip(
        deleteAddress,
        createAddress
      );
    })
    .map(() => new UpdateUserAddressWithNameSuccess())
    .do(() => this.store.dispatch(new GetUserDetails()))
    .catch((err: HttpErrorResponse) => of(new UpdateUserAddressWithNameError(err.message)));

  constructor(private actions: Actions, private afs: AngularFirestore, private store: Store<AppState>) {
  }

  private getUid(): string | null {
    const first = localStorage
      .getItem(`firebase:authUser:${environment.firebaseConfig.apiKey}:Restaurants`);
    return first ? JSON.parse(first).uid : null;
  }
}
