import {Search, GeoCode, Restaurant, User, UserDetails} from '../../core';

export interface AppState {
  user: User;
  geocode: GeoCode;
  restaurant: Restaurant;
  userDetails: UserDetails;
  search: Search;
}
