import {Address} from '../../core/';

export interface IUpdateAddressWithName {
  singleAddress: Address | null;
  deletedName: string | null;
}
