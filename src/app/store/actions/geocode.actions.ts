import {Action} from '@ngrx/store';

import {IGeocode, IGeocodeResponse} from '../../core';


export const GET_GEOCODE = '[GEOCODE] Get Geocode';
export const GET_GEOCODE__SUCCESS = '[GEOCODE] Get Geocode success';
export const GET_GEOCODE__ERROR = '[GEOCODE] Get Geocode error';

export class GetGeocode implements Action {
  readonly type = GET_GEOCODE;

  constructor(public payload: IGeocode) {
  }
}

export class GeoGeocodeSuccess implements Action {
  readonly type = GET_GEOCODE__SUCCESS;

  constructor(public payload: IGeocodeResponse) {
  }
}

export class GetGeocodeError implements Action {
  readonly type = GET_GEOCODE__ERROR;

  constructor(public payload?: string) {
  }
}

export type GEOCODE_ALL
  = GetGeocode
  | GeoGeocodeSuccess
  | GetGeocodeError;
