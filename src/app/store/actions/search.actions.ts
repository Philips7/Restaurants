import {Action} from '@ngrx/store';

import {ISearchResponse} from '../../core';
import {ISearch} from '../../features';


export const GET_SEARCH = '[SEARCH] Get Search';
export const GET_SEARCH__SUCCESS = '[SEARCH] Get Search Success';
export const GET_SEARCH__ERROR = '[SEARCH] Get Search Error';

export class GetSearch implements Action {
  readonly type = GET_SEARCH;

  constructor(public payload: ISearch) {
  }
}

export class GetSearchSuccess implements Action {
  readonly type = GET_SEARCH__SUCCESS;

  constructor(public payload: ISearchResponse) {
  }
}

export class GetSearchError implements Action {
  readonly type = GET_SEARCH__ERROR;

  constructor(public payload: string) {
  }
}

export type Search_ALL
  = GetSearch
  | GetSearchSuccess
  | GetSearchError;
