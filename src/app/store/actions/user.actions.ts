import {Action} from '@ngrx/store';

import {User} from '../../core';


export const GET_USER = '[AUTH] Get user';
export const AUTHENTICATED = '[AUTH] Authenticated';
export const NOT_AUTHENTICATED = '[AUTH] Not Authenticated';
export const GOOGLE_LOGIN = '[AUTH] Google Login attempt';
export const LOGOUT = '[AUTH] Logout';
export const AUTH_ERROR = '[AUTH] Error';

export class GetUser implements Action {
  readonly type = GET_USER;

  constructor(public payload?: any) {
  }
}

export class Authenticated implements Action {
  readonly type = AUTHENTICATED;

  constructor(public payload: User) {
  }
}

export class NotAuthenticated implements Action {
  readonly type = NOT_AUTHENTICATED;
}

export class GoogleLogin implements Action {
  readonly type = GOOGLE_LOGIN;
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export class AuthError implements Action {
  readonly type = AUTH_ERROR;

  constructor(public payload?: string) {
  }
}

export type All
  = GetUser
  | Authenticated
  | NotAuthenticated
  | GoogleLogin
  | Logout
  | AuthError;
