import {Action} from '@ngrx/store';

import {IRestaurant} from '../../features';
import {Restaurant} from '../../core';

export const GET_RESTAURANT = '[RESTAURANT] Get Restaurant';
export const GET_RESTAURANT__SUCCESS = '[RESTAURANT] Get Restaurant success';
export const GET_RESTAURANT__ERROR = '[RESTAURANT] Get Restaurant error';

export class GetRestaurant implements Action {
  readonly type = GET_RESTAURANT;

  constructor(public payload: IRestaurant) {
  }
}

export class GetRestaurantSuccess implements Action {
  readonly type = GET_RESTAURANT__SUCCESS;

  constructor(public  payload: Restaurant) {
  }
}

export class GetRestaurantError implements Action {
  readonly type = GET_RESTAURANT__ERROR;

  constructor(public payload: string) {
  }
}

export type RESTAURANT_ALL
  = GetRestaurant
  | GetRestaurantSuccess
  | GetRestaurantError;
