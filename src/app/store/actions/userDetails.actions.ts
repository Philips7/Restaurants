import {Action} from '@ngrx/store';

import {Address, IUserDetails, UserDetails} from '../../core';
import {IUpdateAddressWithName} from '../interfaces';

export const GET_USER_DETAILS = '[USER DETAILS] Get user data';
export const GET_USER_DETAILS__SUCCESS = '[USER DETAILS] Get user details success';
export const GET_USER_DETAILS__ERROR = '[USER DETAILS] Get user details error';

export const POST_USER_DATA = '[USER DETAILS] Post user data';
export const POST_USER_DATA__SUCCESS = '[USER DETAILS] Post user data success';
export const POST_USER_DATA__ERROR = '[USER DETAILS] Post user data error';

export const POST_USER_ADDRESS = '[USER DETAILS] Post user address';
export const POST_USER_ADDRESS__SUCCESS = '[USER DETAILS] Post user address success';
export const POST_USER_ADDRESS__ERROR = '[USER DETAILS] Post user address error';

export const UPDATE_USER_ADDRESS = '[USER DETAILS] Update user address';
export const UPDATE_USER_ADDRESS__SUCCESS = '[USER DETAILS] Update user address success';
export const UPDATE_USER_ADDRESS__ERROR = '[USER DETAILS] Update user address error';

export const REMOVE_USER_ADDRESS = '[USER DETAILS] Remove user address';
export const REMOVE_USER_ADDRESS__SUCCESS = '[USER DETAILS] Remove user address success';
export const REMOVE_USER_ADDRESS__ERROR = '[USER DETAILS] Remove user address error';

export const UPDATE_USER_ADDRESS_WITH_NAME = '[USER DETAILS] Update user address with name';
export const UPDATE_USER_ADDRESS_WITH_NAME__SUCCESS = '[USER DETAILS] Update user address with name success';
export const UPDATE_USER_ADDRESS_WITH_NAME__ERROR = '[USER DETAILS] Update user address with name error';

export class GetUserDetails implements Action {
  readonly type = GET_USER_DETAILS;

}

export class GetUserDetailsSuccess implements Action {
  readonly type = GET_USER_DETAILS__SUCCESS;

  constructor(public payload: IUserDetails) {
  }
}

export class GetUserDetailsError implements Action {
  readonly type = GET_USER_DETAILS__ERROR;

  constructor(public payload: string) {
  }
}

export class PostUserData implements Action {
  readonly type = POST_USER_DATA;

  constructor(public payload: IUserDetails) {
  }
}

export class PostUserDataSuccess implements Action {
  readonly type = POST_USER_DATA__SUCCESS;
}

export class PostUserDataError implements Action {
  readonly type = POST_USER_DATA__ERROR;

  constructor(public payload: string) {
  }
}

export class PostUserAddress implements Action {
  readonly type = POST_USER_ADDRESS;

  constructor(public payload: Address) {
  }
}

export class PostUserAddressSuccess implements Action {
  readonly type = POST_USER_ADDRESS__SUCCESS;
}

export class PostUserAddressError implements Action {
  readonly type = POST_USER_ADDRESS__ERROR;

  constructor(public payload: string) {

  }
}

export class UpdateUserAddress implements Action {
  readonly type = UPDATE_USER_ADDRESS;

  constructor(public payload: Address) {
  }
}

export class UpdateUserAddressSuccess implements Action {
  readonly type = UPDATE_USER_ADDRESS__SUCCESS;


}

export class UpdateUserAddressError implements Action {
  readonly type = UPDATE_USER_ADDRESS__ERROR;

  constructor(public payload: string) {

  }
}

export class RemoveUserAddress implements Action {
  readonly type = REMOVE_USER_ADDRESS;

  constructor(public payload: string) {
  }
}

export class RemoveUserAddressSuccess implements Action {
  readonly type = REMOVE_USER_ADDRESS__SUCCESS;

}

export class RemoveUserAddressError implements Action {
  readonly type = REMOVE_USER_ADDRESS__ERROR;

  constructor(public payload: string) {
  }
}

export class UpdateUserAddressWithName implements Action {
  readonly type = UPDATE_USER_ADDRESS_WITH_NAME;

  constructor(public payload: IUpdateAddressWithName) {
  }
}

export class UpdateUserAddressWithNameSuccess implements Action {
  readonly type = UPDATE_USER_ADDRESS_WITH_NAME__SUCCESS;

  constructor(public payload?: any) {
  }
}

export class UpdateUserAddressWithNameError implements Action {
  readonly type = UPDATE_USER_ADDRESS_WITH_NAME__ERROR;

  constructor(public payload: string) {
  }
}

export type USER_DETAILS_ALL
  = GetUserDetails
  | GetUserDetailsSuccess
  | GetUserDetailsError
  | PostUserData
  | PostUserDataSuccess
  | PostUserDataError
  | PostUserAddress
  | PostUserAddressSuccess
  | PostUserAddressError
  | UpdateUserAddressError
  | UpdateUserAddress
  | UpdateUserAddressSuccess
  | UpdateUserAddressError
  | RemoveUserAddress
  | RemoveUserAddressSuccess
  | RemoveUserAddressError
  | UpdateUserAddressWithName
  | UpdateUserAddressWithNameSuccess
  | UpdateUserAddressWithNameError;
