import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LoggedGuard} from './shared/guards/logged.guard';

export const routes: Routes = [
  {path: 'restaurants', loadChildren: 'app/features/restaurants/restaurants.module#RestaurantsModule'},
  {path: 'settings', loadChildren: 'app/features/users/users.module#UsersModule', canActivate: [LoggedGuard]},
  {path: '**', redirectTo: '/restaurants', pathMatch: 'full'},
  {path: '', redirectTo: '/restaurants', pathMatch: 'full'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  declarations: [],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
