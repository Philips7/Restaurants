import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {environment} from '@env/environment';

export class ZomatoInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>,
            next: HttpHandler): Observable<HttpEvent<any>> {
    const headerKey = 'user-key';
    const duplicate = req.clone({headers: req.headers.set(`${headerKey}`, `${environment.zomato.apiKey}`)});

    return next.handle(duplicate);
  }

}
