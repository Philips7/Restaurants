import {ILocation, IUserRatings} from '../../features';

export interface IRestaurantResponse {
  id: string;
  name: string;
  location: ILocation;
  average_cost_for_two: string;
  currency: string;
  thumb: string;
  featured_image: string;
  user_rating: IUserRatings;
  cuisines: string;
  all_reviews_count: string;
  phone_numbers: string;
  loading: boolean | null;
  error: string | null;
}

export class Restaurant {
  constructor(public rest: IRestaurantResponse | null) {
  }
}
