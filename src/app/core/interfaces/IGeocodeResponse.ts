import {ILocations, INearbyRestaurants} from '../../features';

export interface IGeocodeResponse {
  link: string | null;
  location: ILocations | null;
  nearby_restaurants: INearbyRestaurants[] | null;
}

export class GeoCode implements IGeocodeResponse {
  constructor(public link: string | null = null, public location: ILocations | null = null,
              public nearby_restaurants: INearbyRestaurants[] | null,
              public loading?: false, public error?: string) {
  }
}
