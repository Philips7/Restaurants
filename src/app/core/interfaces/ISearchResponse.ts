import {INearbyRestaurants} from '../../features';

export interface ISearchResponse {
  results_found: number  | null;
  results_start: number  | null;
  results_shown: number  | null;
  restaurants: INearbyRestaurants[] | null;
}

export class Search implements ISearchResponse {
  constructor(public results_found: number  | null, public results_start: number  | null,
              public results_shown: number  | null, public restaurants: INearbyRestaurants[] | null) {
  }
}
