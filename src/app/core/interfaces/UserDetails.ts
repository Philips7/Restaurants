import {Address, IAddress} from './Address';

export interface IUserDetails {
  name: string | null;
  age: number | null;
  sex: string | null;
  address: IAddress[] | null;
}

export class UserDetails implements IUserDetails {
  name: string | null;
  age: number | null;
  sex: string | null;
  address: Address[] | null;

  constructor(public user: IUserDetails | null) {
  }
}
