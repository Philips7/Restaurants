export interface IGeocode {
  lat: number;
  lon: number;
}
