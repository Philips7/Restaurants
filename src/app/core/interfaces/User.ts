export interface IUser {
  uid: string | null;
  displayName: string | null;
  loading?: boolean;
  error?: string;
}

export class User implements IUser {
  constructor(public uid: string | null, public displayName: string | null) {
  }
}
