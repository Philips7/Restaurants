export interface IAddress {
  name: string;
  street: string;
  city: string;
  state: string;
  zipCode: string;
}

export class Address implements IAddress {
  name: string;
  street: string;
  city: string;
  state: string;
  zipCode: string;

  constructor(public addr: IAddress) {
    this.name =  addr.name;
    this.street =  addr.street;
    this.city =  addr.city;
    this.state =  addr.state;
    this.zipCode =  addr.zipCode;

  }
}
