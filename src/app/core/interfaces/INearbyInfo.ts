export interface INearbyInfo {
  results_found: number;
  results_start: number;
  results_shown: number;
}
