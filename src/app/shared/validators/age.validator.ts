import {AbstractControl} from '@angular/forms';

export function ValidateAge(control: AbstractControl): {[key: string]: boolean} | null {
  const age = parseInt(control.value, 10);
  if ( (control.value != undefined && (isNaN(control.value))) ||  (age < 3 || age > 115)) {
      return {'age' : true};
  }
  return null;
}
