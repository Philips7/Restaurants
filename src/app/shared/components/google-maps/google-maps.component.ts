import {
  ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostListener, Input, NgZone, OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';
import {AgmMap, MapsAPILoader} from '@agm/core';
import {FormControl} from '@angular/forms';
import {} from '@types/googlemaps';

import {AppState, GetGeocode, GetSearch, selectGeocodeAll, selectSearchAll} from '../../../store';
import {Search, GeoCode} from '../../../core';
import {MapStyle} from './map.style';

@Component({
  selector: 'app-google-maps',
  templateUrl: './google-maps.component.html',
  styleUrls: ['./google-maps.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GoogleMapsComponent implements OnInit {

  readonly LAT = 40.730610;
  readonly LON = -73.935242;
  readonly customStyle = MapStyle;

  geocode$: Observable<GeoCode>;
  search$: Observable<Search>;
  searchControl: FormControl;
  @ViewChild(AgmMap) myMap: any;
  @Output() location = new EventEmitter();
  @Input() topTen: boolean;
  shadow: object;
  border: object;
  lat: number;
  lon: number;

  @ViewChild('search')
  searchElementRef: ElementRef;

  constructor(private mapsAPILoader: MapsAPILoader,
              private ngZone: NgZone,
              private store: Store<AppState>,
              private router: Router) {

    this.geocode$ = this.store.select(selectGeocodeAll);
    this.search$ = this.store.select(selectSearchAll);
    this.searchControl = new FormControl();
    this.topTen = false;
    this.setBasicStyles();
    this.setLocation();
    this.setUserLocation();
  }

  ngOnInit() {
    this.mapsAPILoader.load().then(() => {
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.setLocation(place.geometry.location.lat(), place.geometry.location.lng());
          this.onWindowResize();
          this.location.emit({lat: this.lat, lon: this.lon});
          this.store.dispatch(new GetGeocode({lat: this.lat, lon: this.lon}));
          this.store.dispatch(new GetSearch({lat: this.lat, lon: this.lon}));
        });
      });
    });
    this.location.emit({lat: this.lat, lom: this.lon});
    this.store.dispatch(new GetSearch({lat: this.lat, lon: this.lon}));
    this.store.dispatch(new GetGeocode({lat: this.lat, lon: this.lon}));
  }

  @HostListener('window:resize')
  private onWindowResize() {
    this.myMap.triggerResize()
      .then(() => this.myMap._mapsWrapper.setCenter({lat: this.lat, lng: this.lon}));
  }

  private setLocation(lat = this.LAT, lon = this.LON) {
    this.lat = lat;
    this.lon = lon;
  }

  private setBasicStyles() {
    this.shadow = {
      blur: '10px',
      spread: '10px',
      opacity: 0.5,
      color: '#2B3031'
    };
    this.border = {
      width: '5px',
      color: '#2B3031'
    };
  }

  navigateToDetails(id: string) {
    this.router.navigateByUrl(`/restaurants/${id}`);
  }

  private setUserLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(position => {
        this.lat = position.coords.latitude;
        this.lon = position.coords.longitude;
        this.store.dispatch(new GetGeocode({lat: this.lat, lon: this.lon}));
        this.store.dispatch(new GetSearch({lat: this.lat, lon: this.lon}));
        this.location.emit({lat: this.lat, lon: this.lon});
      });
    }
  }

}
