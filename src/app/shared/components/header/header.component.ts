import {ChangeDetectionStrategy, Component} from '@angular/core';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {AppState, selectUserAll, GetUser, GoogleLogin, Logout} from '../../../store';
import {User} from '../../../core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {

  user$: Observable<User>;
  navCloseStyle = {
    'width': 0 + 'px'
  };
  markdownStyle = {
    'visibility': 'hidden'
  };

  isNavOpen: boolean;

  constructor(private store: Store<AppState>) {
    this.user$ = this.store.select(selectUserAll);
    this.store.dispatch(new GetUser());
  }

  googleLogin() {
    this.store.dispatch(new GoogleLogin());
  }

  logout() {
    this.store.dispatch(new Logout());
  }

  manageNavStyle() {
    if (this.isNavOpen) {
      this.isNavOpen = false;
      this.navCloseStyle = {
        'width': 0 + 'px'
      };
      this.markdownStyle = {
        'visibility': 'hidden'
      };
    } else {
      this.isNavOpen = true;
      this.navCloseStyle = {
        'width': 250 + 'px'
      };
      this.markdownStyle = {
        'visibility': 'visible'
      };
    }
  }

}
