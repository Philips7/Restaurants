import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve} from '@angular/router';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs/Observable';

import {AppState, GetRestaurant, selectRestaurant} from '../../store';
import {Restaurant} from '../../core';

@Injectable()
export class RestaurantResolver implements Resolve<any> {

  constructor(private store: Store<AppState>) {
  }

  resolve(route: ActivatedRouteSnapshot): Observable<Restaurant> {
    return this.getRestaurantById(route.params.id)
      .map(response => response)
      .filter(data => data.rest !== null || data['error'] !== null)
      .take(1);
  }

  private getRestaurantById(id: number): Observable<Restaurant> {
    this.store.dispatch(new GetRestaurant({res_id: id}));
    return this.store.select(selectRestaurant);
  }

}
