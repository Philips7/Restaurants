import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';
import {AgmSnazzyInfoWindowModule} from '@agm/snazzy-info-window';

import {HeaderComponent, FooterComponent, GoogleMapsComponent} from './components';
import { ToNumberPipe } from './pipes';
import { LoggedInDirective, LoggedOutDirective } from './directives';
import {environment} from '@env/environment';
import {LoggedGuard} from './guards/logged.guard';

@NgModule({
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: environment.googleMapsKey,
      libraries: ['places']
    }),
    AgmSnazzyInfoWindowModule,
    RouterModule,
    ReactiveFormsModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    GoogleMapsComponent,
    ToNumberPipe,
    LoggedInDirective,
    LoggedOutDirective
  ],
  exports: [
    CommonModule,
    FormsModule,
    HeaderComponent,
    FooterComponent,
    GoogleMapsComponent,
    ToNumberPipe
  ],
  providers: [
    LoggedInDirective,
    LoggedOutDirective,
    LoggedGuard
  ]
})
export class SharedModule {
}
