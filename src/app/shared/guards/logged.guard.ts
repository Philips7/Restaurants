import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {environment} from '@env/environment';

@Injectable()
export class LoggedGuard implements CanActivate {

  constructor(private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {

    const first = localStorage
      .getItem(`firebase:authUser:${environment.firebaseConfig.apiKey}:Restaurants`);
    if (first) {
      const exists = !!JSON.parse(first).uid;
      if (exists) {
        return true;
      } else {
        this.router.navigate(['restaurants']);
        return false;
      }
    }
    this.router.navigate(['restaurants']);
    return false;
  }
}
