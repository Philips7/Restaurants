import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {Store} from '@ngrx/store';

import {AppState, selectUserAll} from '../../store';

@Directive({
  selector: '[appLoggedOut]'
})
export class LoggedOutDirective {

  constructor(private templateRef: TemplateRef<any>,
              private viewContainerRef: ViewContainerRef,
              private store: Store<AppState>) {
    this.store.select(selectUserAll).subscribe(user => {
      if (!user.uid) {
        this.viewContainerRef.clear();
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
        this.viewContainerRef.clear();
      }
    });
  }

}
