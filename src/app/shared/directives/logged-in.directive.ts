import {Directive, TemplateRef, ViewContainerRef} from '@angular/core';
import {Store} from '@ngrx/store';

import {AppState, selectUserAll} from '../../store';

@Directive({
  selector: '[appLoggedIn]'
})
export class LoggedInDirective {

  constructor(private templateRef: TemplateRef<any>,
              private viewContainerRef: ViewContainerRef,
              private store: Store<AppState>) {
    this.store.select(selectUserAll).subscribe(user => {
      if (user.uid) {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
      } else {
        this.viewContainerRef.createEmbeddedView(this.templateRef);
        this.viewContainerRef.clear();
      }
    });
  }

}
