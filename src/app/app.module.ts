import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {environment} from '@env/environment';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule} from 'angularfire2/auth';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFirestoreModule} from 'angularfire2/firestore';

import {AppRoutingModule} from './app-routing.module';
import {CoreModule} from './core';
import {FeaturesModule} from './features';
import {SharedModule} from './shared';
import {AppComponent} from './app.component';
import {
  GeocodeEffects,
  geocodeReducer,
  RestaurantEffects,
  restaurantReducer,
  SearchEffects,
  searchReducer,
  UserDetailsEffects,
  userDetailsReducer,
  UserEffects,
  userReducer
} from './store';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    AngularFireAuthModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(environment.firebaseConfig, 'Restaurants'),
    AngularFirestoreModule,
    AppRoutingModule,
    FeaturesModule,
    SharedModule,
    EffectsModule.forRoot([
      UserEffects,
      GeocodeEffects,
      RestaurantEffects,
      UserDetailsEffects,
      SearchEffects
    ]),
    StoreModule.forRoot({
      user: userReducer,
      geocode: geocodeReducer,
      restaurant: restaurantReducer,
      userDetails: userDetailsReducer,
      search: searchReducer,
    }),
    StoreDevtoolsModule.instrument({
      maxAge: 10
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() {
  }

}
