// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyD8urDkBj2lTr4FP0t02sywGayuZ3l_OhQ',
    authDomain: 'restaurants-dev.firebaseapp.com',
    databaseURL: 'https://restaurants-dev.firebaseio.com',
    projectId: 'restaurants-dev',
    storageBucket: '',
    messagingSenderId: '821217745289'
  },
  zomato: {
    apiKey: '36b64a2a607a69ff0b7cd362727d9008',
    url: 'https://developers.zomato.com/api/v2.1'
  },
  googleMapsKey: 'AIzaSyBd-HtbyOmW2PzG5zLvEmVh4WNv_DA7zq4'
};


